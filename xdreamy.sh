#!/bin/sh

# Configuration
#########################################
plugin="xdreamy"
git_url="https://gitlab.com/hmeng80/skin-all/-/raw/main/xDreamy"
version=$(wget $git_url/version -qO- | awk 'NR==1')
plugin_path="/usr/share/enigma2/xDreamy"
package="enigma2-plugin-skins-$plugin"
targz_file="$plugin.tar.gz"
url="$git_url/$targz_file"
temp_dir="/tmp"

# Determine package manager
#########################################
if command -v dpkg &> /dev/null; then
package_manager="apt"
status_file="/var/lib/dpkg/status"
uninstall_command="apt-get purge --auto-remove -y"
else
package_manager="opkg"
status_file="/var/lib/opkg/status"
uninstall_command="opkg remove --force-depends"
fi

#check and_remove package old version
#########################################
check_and_remove_package() {
if [ -d $plugin_path ]; then
echo "> removing package old version please wait..."
sleep 3 
files_to_remove=(
    /usr/bin/opbitrate
    /usr/share/enigma2/xDreamy
    /usr/lib/enigma2/python/Plugins/Extensions/xDreamy
    /usr/lib/enigma2/python/Components/Converter/iAccess.py
    /usr/lib/enigma2/python/Components/Converter/iBase.py
    /usr/lib/enigma2/python/Components/Converter/iBitrate.py
    /usr/lib/enigma2/python/Components/Converter/iBitrate3.py
    /usr/lib/enigma2/python/Components/Converter/iBoxInfo.py
    /usr/lib/enigma2/python/Components/Converter/iCaidInfo2.py
    /usr/lib/enigma2/python/Components/Converter/iCamdRAED.py
    /usr/lib/enigma2/python/Components/Converter/iCpuUsage.py
    /usr/lib/enigma2/python/Components/Converter/iCryptoInfo.py
    /usr/lib/enigma2/python/Components/Converter/iEcmInfo.py
    /usr/lib/enigma2/python/Components/Converter/iEventList.py
    /usr/lib/enigma2/python/Components/Converter/iEventName2.py
    /usr/lib/enigma2/python/Components/Converter/iExtra.py
    /usr/lib/enigma2/python/Components/Converter/iExtraNumText.py
    /usr/lib/enigma2/python/Components/Converter/iFrontendInfo.py
    /usr/lib/enigma2/python/Components/Converter/iInfoEvent.py
    /usr/lib/enigma2/python/Components/Converter/iMenuDescription.py
    /usr/lib/enigma2/python/Components/Converter/iMenuEntryCompare.py
    /usr/lib/enigma2/python/Components/Converter/iNetSpeedInfo.py
    /usr/lib/enigma2/python/Components/Converter/iNextEvents.py
    /usr/lib/enigma2/python/Components/Converter/iReceiverInfo.py
    /usr/lib/enigma2/python/Components/Converter/iRouteInfo.py
    /usr/lib/enigma2/python/Components/Converter/iServName2.py
    /usr/lib/enigma2/python/Components/Converter/iTemp.py
    /usr/lib/enigma2/python/Components/Converter/iVpn.py
    /usr/lib/enigma2/python/Components/Renderer/iBackdropX.py
    /usr/lib/enigma2/python/Components/Renderer/iBackdropXDownloadThread.py
    /usr/lib/enigma2/python/Components/Renderer/iChannelNumber.py
    /usr/lib/enigma2/python/Components/Renderer/iEmptyEpg.py
    /usr/lib/enigma2/python/Components/Renderer/iEventListDisplay.py
    /usr/lib/enigma2/python/Components/Renderer/iGenre.py
    /usr/lib/enigma2/python/Components/Renderer/iInfoEvents.py
    /usr/lib/enigma2/python/Components/Renderer/iNxtEvnt.py
    /usr/lib/enigma2/python/Components/Renderer/iPosterX.py
    /usr/lib/enigma2/python/Components/Renderer/iPosterXDownloadThread.py
    /usr/lib/enigma2/python/Components/Renderer/iPosterXEMC.py
    /usr/lib/enigma2/python/Components/Renderer/iRunningText.py
    /usr/lib/enigma2/python/Components/Renderer/iSingleEpgList.py
    /usr/lib/enigma2/python/Components/Renderer/iStarX.py
    /usr/lib/enigma2/python/Components/Renderer/iVolume2.py
    /usr/lib/enigma2/python/Components/Renderer/iVolumeText.py
    /usr/lib/enigma2/python/Components/Renderer/iVolz.py
    /usr/lib/enigma2/python/Components/Renderer/iWatches.py
    /usr/lib/enigma2/python/Components/Converter/iServicePosition.py
)

for file in "${files_to_remove[@]}"; do
    rm -rf "$file" > /dev/null 2>&1
done

if grep -q "$package" "$status_file"; then
echo "> Removing existing $package package, please wait..."
$uninstall_command $package > /dev/null 2>&1
fi
echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Haitham          *"
echo "*******************************************"
sleep 3
exit 1
else
echo " " 
fi  }
check_and_remove_package

#check & install dependencies
#########################################
check_and_install_dependencies() {
# Determine package manager
if command -v dpkg &> /dev/null; then
    install_command="apt-get install"
else
    install_command="opkg install"
fi
$install_command enigma2-plugin-extensions-bitrate enigma2-plugin-extensions-oaweather >/dev/null 2>&1
}
check_and_install_dependencies

#download & install package
#########################################
download_and_install_package() {
echo "> Downloading $plugin-$version package  please wait ..."
sleep 3
wget --show-progress -qO $temp_dir/$targz_file --no-check-certificate $url
tar -xzf $temp_dir/$targz_file -C / > /dev/null 2>&1
extract=$?
rm -rf $temp_dir/$targz_file >/dev/null 2>&1

if [ $extract -eq 0 ]; then

SKINDIR='/usr/share/enigma2/xDreamy'
TMPDIR='/tmp'
set -e
if grep -qs -i "openATV" /etc/image-version; then
    cp $SKINDIR/image_logo/openatv/imagelogo.png $SKINDIR

elif grep -qs -i "egami" /etc/image-version; then
    cp $SKINDIR/image_logo/egami/imagelogo.png $SKINDIR

elif grep -qs -i "PURE2" /etc/image-version; then
    cp $SKINDIR/image_logo/pure2/imagelogo.png $SKINDIR

elif grep -qs -i "OpenSPA" /etc/image-version; then
    cp $SKINDIR/image_logo/openspa/imagelogo.png $SKINDIR

elif grep -qs -i "OpenDroid" /etc/image-version; then
    cp $SKINDIR/image_logo/opendroid/imagelogo.png $SKINDIR

elif grep -qs -i "openvix" /etc/image-version; then
    cp $SKINDIR/image_logo/openvix/imagelogo.png $SKINDIR
    
elif grep -qs -i "openHDF" /etc/image-version; then
    cp $SKINDIR/image_logo/HDF/imagelogo.png $SKINDIR
    
elif grep -qs -i "openbh" /etc/issue; then
    cp $SKINDIR/image_logo/openbh/imagelogo.png $SKINDIR

elif grep -qs -i "openpli" /etc/issue; then
    cp $SKINDIR/image_logo/openpli/imagelogo.png $SKINDIR

elif grep -qs -i "nonsolosat" /etc/issue; then
    cp $SKINDIR/image_logo/NSS/imagelogo.png $SKINDIR/imagelogo.png
    
elif grep -qs -i "satlodge" /etc/issue; then
    cp $SKINDIR/image_logo/satlodge/imagelogo.png $SKINDIR/imagelogo.png

else
    echo
fi
sleep 2

# Identify the box type from the hostname file
box_type=$(head -n 1 /etc/hostname)

# Check if the box type image exists
if [[ -f "/usr/share/enigma2/$box_type.png" ]]; then
    # Copy the matched image to the skin directory
    cp "/usr/share/enigma2/$box_type.png" "$SKINDIR/boximage.png"
else
    echo
fi

# Create an error debug file in /tmp/
echo "Debug information:" > /tmp/xdreamy_debug.txt
echo "Box model: $box_type" >> /tmp/xdreamy_debug.txt
echo "Image filename: $IMAGE_FILENAME" >> /tmp/xdreamy_debug.txt
echo "   "
rm -rf "$SKINDIR/image_logo" > /dev/null 2>&1
rm -rf /control > /dev/null 2>&1
echo "   "
sleep 2
rm -rf /var/volatile/tmp/*.ipk > /dev/null 2>&1

  echo "> $plugin-$version package installed successfully"
  sleep 3
  echo ""
else
  echo "> $plugin-$version package download failed"
  sleep 3
fi  }
download_and_install_package

# Remove unnecessary files and folders
#########################################
print_message() {
echo "> [$(date +'%Y-%m-%d')] $1"
}
cleanup() {
[ -d "/CONTROL" ] && rm -rf /CONTROL >/dev/null 2>&1
rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1
print_message "> Uploaded By Haitham "
}
cleanup
    