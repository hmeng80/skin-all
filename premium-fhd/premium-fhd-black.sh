#!/bin/sh

if [ -d /usr/share/enigma2/Premium-FHD-Black ]; then
echo "> removing package please wait..."
sleep 3s 
rm -rf /usr/share/enigma2/Premium-FHD-Black >/dev/null 2>&1

echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Haitham          *"
echo "*******************************************"
sleep 3s

else

# Remove unnecessary files and folders
[ -d "/CONTROL" ] && rm -r /CONTROL >/dev/null 2>&1
rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1

deps=("enigma2-plugin-extensions-bitrate" "enigma2-plugin-extensions-oaweather" "enigma2-plugin-extensions-bitrate")

left=">>>>"
right="<<<<"
LINE1="---------------------------------------------------------"
LINE2="-------------------------------------------------------------------------------------"

if [ -f /etc/opkg/opkg.conf ]; then
  STATUS='/var/lib/opkg/status'
  OSTYPE='Opensource'
  OPKG='opkg update'
  OPKGINSTAL='opkg install'
elif [ -f /etc/apt/apt.conf ]; then
  STATUS='/var/lib/dpkg/status'
  OSTYPE='DreamOS'
  OPKG='apt-get update'
  OPKGINSTAL='apt-get install -y'
fi

install() {
  if ! grep -qs "Package: $1" "$STATUS"; then
    $OPKG >/dev/null 2>&1
    rm -rf /run/opkg.lock
    echo -e "> Need to install ${left} $1 ${right} please wait..."
    echo "$LINE2"
    sleep 0.8
    echo
    if [ "$OSTYPE" = "Opensource" ]; then
      $OPKGINSTAL "$1"
      sleep 1
      clear
    elif [ "$OSTYPE" = "DreamOS" ]; then
      $OPKGINSTAL "$1" -y
      sleep 1
      clear
    fi
  fi
}

for i in "${deps[@]}"; do
  install "$i"
done

#config
skin=premium-fhd-black
version=3.1
url=https://gitlab.com/hmeng80/skin-all/-/raw/main/premium-fhd/premium-fhd-black-3.1.tar.gz
package=/var/volatile/tmp/$skin-$version.tar.gz

echo "> Downloading $skin-$version skin  please wait ..."
sleep 3s

wget -O $package --no-check-certificate $url
tar -xzf $package -C /
extract=$?
rm -rf $package >/dev/null 2>&1

echo ''
if [ $extract -eq 0 ]; then
SKINDIR='/usr/share/enigma2/Premium-FHD-Black'
TMPDIR='/tmp'
set -e
if grep -qs -i "openATV" /etc/image-version; then
mv $SKINDIR/image_logo/openatv/imagelogo.png $SKINDIR
elif grep -qs -i "egami" /etc/image-version; then
mv $SKINDIR/image_logo/egami/imagelogo.png $SKINDIR
elif grep -qs -i "PURE2" /etc/image-version; then
mv $SKINDIR/image_logo/pure2/imagelogo.png $SKINDIR
elif grep -qs -i "OpenSPA" /etc/image-version; then
mv $SKINDIR/image_logo/openspa/imagelogo.png $SKINDIR
else
echo ""
fi

sleep 2
mv -f $SKINDIR/pfiles/premiumcomponents.tar.gz $TMPDIR
cd $TMPDIR
tar -xzf premiumcomponents.tar.gz
cp -r usr /
cd ..
sleep 2
rm -rf $SKINDIR/pfiles  > /dev/null 2>&1
rm -rf $SKINDIR/image_logo  > /dev/null 2>&1
rm -rf /CONTROL  > /dev/null 2>&1
rm -r $TMPDIR/premiumcomponents.tar.gz > /dev/null 2>&1
rm -rf $TMPDIR/usr > /dev/null 2>&1
set +e
sleep 2

echo "> $skin-$version skin installed successfully"
echo "> Uploaded By Haitham "
sleep 3s
else
echo "> $skin-$version skin installation failed"
sleep 3s
fi
fi
