#!/bin/sh
echo ''
echo '************************************************************'
echo "**                         STARTED                        **"
echo '************************************************************'
echo "**                 Uploaded by: Haitham                   **"
echo "************************************************************"
echo ''
sleep 3s

if [ -d /usr/share/enigma2/Artemis_mod_dragon ]; then
echo "> removing package please wait..."
sleep 3s 
rm -rf /usr/share/enigma2/Artemis_mod_dragon > /dev/null 2>&1

status='/var/lib/opkg/status'
package='enigma2-plugin-skins-artemis-mod-dragon'

if grep -q $package $status; then
opkg remove $package > /dev/null 2>&1
fi

echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Haitham          *"
echo "*******************************************"
sleep 3s

else

#remove unnecessary files and folders
if [  -d "/CONTROL" ]; then
rm -r  /CONTROL >/dev/null 2>&1
fi
rm -rf /control >/dev/null 2>&1
rm -rf /postinst >/dev/null 2>&1
rm -rf /preinst >/dev/null 2>&1
rm -rf /prerm >/dev/null 2>&1
rm -rf /postrm >/dev/null 2>&1
rm -rf /tmp/*.ipk >/dev/null 2>&1
rm -rf /tmp/*.tar.gz >/dev/null 2>&1

status='/var/lib/opkg/status'
package='enigma2-plugin-skins-artemis-mod-dragon'
package1='enigma2-plugin-skins-artemis-mod-dragon'
if grep -q $package $status; then
opkg install $package > /dev/null 2>&1
fi
if grep -q $package1 $status; then
opkg install $package1 > /dev/null 2>&1
fi

#config
plugin=artemis-mod-dragon
version=7.7.poster.backdrop
url=https://gitlab.com/hmeng80/skin-all/-/raw/main/skins-artemis-mod-dragon_7.7.poster.backdrop.tar.gz
package=/var/volatile/tmp/$plugin-$version.tar.gz

#download & install
echo "> Downloading $plugin-$version package  please wait ..."
sleep 3s

wget -O $package --no-check-certificate $url
tar -xzf $package -C /
extract=$?
rm -rf $package >/dev/null 2>&1

echo ''
if [ $extract -eq 0 ]; then
SKINDIR='/usr/share/enigma2/Artemis_mod_dragon'
TMPDIR='/tmp'
set -e
if grep -qs -i "openATV" /etc/image-version; then
mv $SKINDIR/image_logo/openatv/imagelogo.png $SKINDIR
   
elif grep -qs -i "egami" /etc/image-version; then
mv $SKINDIR/image_logo/egami/imagelogo.png $SKINDIR
	
elif grep -qs -i "PURE2" /etc/image-version; then
mv $SKINDIR/image_logo/pure2/imagelogo.png $SKINDIR
	
elif grep -qs -i "OpenSPA" /etc/image-version; then
mv $SKINDIR/image_logo/openspa/imagelogo.png $SKINDIR
	
else
echo ""
fi
sleep 2
rm -rf $SKINDIR/image_logo  > /dev/null 2>&1
rm -rf /control  > /dev/null 2>&1
sleep 2
set +e
echo "> $plugin-$version package installed successfully"
echo "> Uploaded By Haitham "
sleep 3s

else

echo "> $plugin-$version package installation failed"
sleep 3s
fi

fi
