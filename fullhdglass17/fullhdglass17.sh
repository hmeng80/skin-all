#!/bin/sh

# Configuration
#########################################
plugin="fullhdglass17"
git_url="https://gitlab.com/hmeng80/skin-all/-/raw/main/fullhdglass17"
version=$(wget $git_url/version -qO- | awk 'NR==1')
plugin_path="/usr/share/enigma2/hd_glass17"
package="enigma2-plugin-skins-$plugin"
targz_file="$plugin.tar.gz"
url="$git_url/$targz_file"
temp_dir="/tmp"

# Determine package manager
#########################################
if command -v dpkg &> /dev/null; then
package_manager="apt"
status_file="/var/lib/dpkg/status"
uninstall_command="apt-get purge --auto-remove -y"
else
package_manager="opkg"
status_file="/var/lib/opkg/status"
uninstall_command="opkg remove --force-depends"
fi

#check and_remove package old version
#########################################
check_and_remove_package() {
if [ -d $plugin_path ]; then
echo "> removing package old version please wait..."
sleep 3 
rm -rf $plugin_path > /dev/null 2>&1
SCREENPATH=/usr/lib/enigma2/python/Screens
COMPPATH=/usr/lib/enigma2/python/Components
SKINPATH=/usr/share/enigma2/hd_glass17
PLUGINPATH=/usr/lib/enigma2/python/Plugins/Extensions/setupGlass17

cp -f /etc/enigma2/skin_user-ori.xml /etc/enigma2/skin_user.xml
cp -f /usr/share/enigma2/encoding-ori.conf /usr/share/enigma2/encoding.conf
cp -f $SCREENPATH/InfoBarGenerics-ori.py $SCREENPATH/InfoBarGenerics.py
cp -f $SCREENPATH/ChannelSelection-ori.py $SCREENPATH/ChannelSelection.py
cp -f /usr/share/enigma2/keymap-ori.xml /usr/share/enigma2/keymap.xml
cp -f $SCREENPATH/Menu-orig17.py $SCREENPATH/Menu.py
cp -f $SCREENPATH/Menu-orig17.pyo $SCREENPATH/Menu.pyo
cp -f $SCREENPATH/Menu-orig17.pyc $SCREENPATH/Menu.pyc

rm -rf /usr/bin/btrGen
rm -rf /usr/bin/hdd_temp_hdg
rm -rf /usr/share/enigma2/encoding-ori.conf
rm -rf /etc/enigma2/skin_user-ori.xml
rm -rf /usr/share/enigma2/keymap-ori.xml
rm -rf -f $PLUGINPATH/* 
rmdir $PLUGINPATH
rm -rf -f $COMPPATH/g17*
rm -rf -f $COMPPATH/*-new17.*
rm -rf -f $COMPPATH/*-orig.*
rm -rf -f $COMPPATH/Converter/g17*
rm -rf -f $COMPPATH/Renderer/g17*
rm -rf -f $SCREENPATH/g17*
rm -rf -f $SCREENPATH/*-new17.*
rm -rf -f $SCREENPATH/*-ori*
rm -rf -f $SCREENPATH/G17*
rm -rf -f $SKINPATH/* 
rmdir $SKINPATH

if grep -q "$package" "$status_file"; then
echo "> Removing existing $package package, please wait..."
$uninstall_command $package > /dev/null 2>&1
fi
echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Haitham          *"
echo "*******************************************"
sleep 3
exit 1
else
echo " " 
fi  }
check_and_remove_package

if grep -q parameters /usr/share/enigma2/skin_default.xml; then
 Image_version=ok 
elif grep -q alias /usr/share/enigma2/skin_default.xml; then
 Image_version=ok
elif grep -q alias /usr/share/enigma2/skin.xml; then
 Image_version=ok
elif grep -q alias /usr/share/enigma2/skin_default/skin.xml; then
 Image_version=ok
else
  echo "Unsupported image version detected, sorry, this skin is unusable for this image"
  exit 1
fi

#download & install package
#########################################
download_and_install_package() {
echo "> Downloading $plugin-$version package  please wait ..."
sleep 3
wget --show-progress -qO $temp_dir/$targz_file --no-check-certificate $url
tar -xzf $temp_dir/$targz_file -C / > /dev/null 2>&1
extract=$?
rm -rf $temp_dir/$targz_file >/dev/null 2>&1

if [ $extract -eq 0 ]; then
end_inst () {
   rm -rf $SCREENPATH/Menu-new17-38.py
   rm -rf /usr/share/misc/hddtemp-deff17.db
   rm -rf /etc/hddtemp-deff17.db
   cp -f /etc/lcstrings-def17.list /etc/lcstrings.list
   rm -rf /etc/lcstrings-def17.list
   cp -f /etc/city_Code-17.txt /etc/city_Code.txt
   rm -rf /etc/city_Code-17.txt
   cp -f /etc/iptvprov-def17.list /etc/iptvprov.list
   rm -rf /etc/iptvprov-def17.list
   if [ `ls -al /etc | grep bhversion | wc -l` -gt 0 ]; then
      echo "BH detected ..."
      cp -f $PLUGINSPATH/SkinSelector/plugin.py $PLUGINSPATH/SkinSelector/plugin-ori.py
      cp -f /tmp/plugin-17.py $PLUGINSPATH/SkinSelector/plugin.py
   fi
   rm -rf /tmp/plugin-17.py
}

SCREENPATH=/usr/lib/enigma2/python/Screens
PLUGINSPATH=/usr/lib/enigma2/python/Plugins/SystemPlugins
if [ `ls -al /usr/lib/python3.* | grep base64.py | wc -l` -gt 0 ]; then
 cp -f $SCREENPATH/Menu-new17-38.py $SCREENPATH/Menu-new17.py
fi
if [ `ls -al /usr/share/misc | grep hddtemp.db | wc -l` -gt 0 ]; then
e=1
else
 cp -f /usr/share/misc/hddtemp-deff17.db /usr/share/misc/hddtemp.db
fi
if [ `ls -al /etc | grep hddtemp.db | wc -l` -gt 0 ]; then
e=2
else
 cp -f /etc/hddtemp-deff17.db /etc/hddtemp.db
fi
end_inst

  echo "> $plugin-$version package installed successfully"
  sleep 3
  echo ""
else
  echo "> $plugin-$version package download failed"
  sleep 3
fi  }
download_and_install_package

# Remove unnecessary files and folders
#########################################
print_message() {
echo "> [$(date +'%Y-%m-%d')] $1"
}
cleanup() {
[ -d "/CONTROL" ] && rm -rf /CONTROL >/dev/null 2>&1
rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1
print_message "> Uploaded By Haitham "
}
cleanup
    